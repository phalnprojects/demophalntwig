<?php
require_once '../config/testsConfig.php';

use Phaln\BDD;

try
{
    $db = BDD::get_bdd();
    var_dump($db);
    
    $con = $db->get_connexion();
    var_dump($con);

    $attributes = array(
		    "AUTOCOMMIT", "ERRMODE", "CASE", "CLIENT_VERSION", "CONNECTION_STATUS",
		    "ORACLE_NULLS", "PERSISTENT", "SERVER_INFO", "SERVER_VERSION",
		);
    
    foreach ($attributes as $val) {
	echo "PDO::ATTR_$val: ";
	echo $con->getAttribute(constant("PDO::ATTR_$val")) . "<br/>";
    }
}
catch (Throwable $e)
{
    echo 'Dans TestBdd:<br/>'.$e->getMessage().'<br/>';
    var_dump($e->getTrace());
    echo $e->getPrevious().'<br/>';
}
