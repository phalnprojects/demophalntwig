<?php
require_once '../config/testsConfig.php';

use App\Entities\Personne;
use App\Repositories\PersonneRepository;

function testGetAll() {
    $mapper = new PersonneRepository();
    echo '<h1>getAll()</h1>';
    echo '<h3>test 1</h3>';
    $res = $mapper->getAll();
    dump_var($res, true, 'getAll()');
    echo '<h3>test 2</h3>';
    $res = $mapper->getAll(['prenom' => 'ASC', 'nom' => 'DESC']);
    dump_var($res, true, "getAll(['prenom' => 'ASC', 'nom' => 'DESC'])");    
}

function testGetById() {
    $mapper = new PersonneRepository();
    echo '<h1>getById()</h1>';
    echo '<h3>test 1</h3>';
    $res = $mapper->getById(11);
    dump_var($res, true, 'getById(11)');
    echo '<h3>test 2</h3>';
    $res = $mapper->getById(99);
    dump_var($res, true, 'getById(99)');
    echo '<h3>test 3</h3>';
    $res = $mapper->getById(-1);
    dump_var($res, true, 'getById(-1)');
}

function testSave() {
    $mapper = new PersonneRepository();
    echo '<h1>sauver()</h1>';
    $datas = array(
		'nom' => 'Play',
		'prenom' => 'Henris',
    );
    $entity = new Personne($datas);
    dump_var($entity);
    echo '<h3>test 1</h3>';
    $res = $mapper->sauver($entity);
    dump_var($res);
    echo '<h3>test 2</h3>';
    $res->setnom('Modif');
    $res->setid_pers(666);
    dump_var($res);
    $res = $mapper->sauver($res);
    dump_var($res);
}


function testDeleteEntity()
{
    $mapper = new PersonneRepository();
    echo '<h1>DeleteEntity()</h1>';
    echo '<h4>T1</h4>';
    $entity = new Personne(array('id_pers'=>50));
    dump_var($entity);
    if($mapper->exist($entity)) {
	$res = $mapper->deleteEntity($entity);
	dump_var($res, DUMP, 'Delete existe:');
    }
    else {
	$res = $mapper->deleteEntity($entity);
	dump_var($res, DUMP, 'Delete n\'existe pas:');
	$entS = $mapper->save($entity);
	$res = $mapper->deleteEntity($entS);
	dump_var($res, DUMP, 'Delete existe bis :');
    }
}

function testGetBy()
{
    $mapper = new PersonneRepository();
    $by = [
	    ['fieldName' => 'nom', 'comp'=>'LIKE', 'value'=>'%TO%'], 
	    ['op'=>'OR', 'fieldName' => 'prenom', 'comp'=>'=', 'value'=>'Machine']
	];
    echo '<h3>test 1</h3>';
    $res = $mapper->getBy($by);
    dump_var($res, true, 'getBy($by):');
    echo '<h3>test 2</h3>';
    $res = $mapper->getBy($by, ['prenom' => 'ASC', 'nom' => 'DESC']);
    dump_var($res, true, "getBy(by, ['prenom' => 'ASC', 'nom' => 'DESC']):");
}

testGetAll();
testGetById();
testGetBy();
testSave();
testDeleteEntity();
