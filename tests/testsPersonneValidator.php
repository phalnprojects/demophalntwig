<?php
require_once '../config/testsConfig.php';

use App\Entities\Personne;

echo '<h1>Création Personne</h1>';
$datas = array(
	    'id_pers' => 22,
	    'nom' => 'Plaie',
	    'prenom' => 'Henry',
);

$validator = new App\Validators\PersonneValidator();
var_dump($validator);

echo '<h3>Création Personne avec données valides directement</h3>';
$entity = new Personne($datas);
var_dump($entity);
echo '<h3>Création Personne avec données valides via le validator</h3>';
$entityV = $validator->createValidEntity($datas);
var_dump($entityV);

echo '<h3>Création Personne avec données invalides-correctes directement</h3>';
$datas2 = array(
	    'id_pers' => '66',
	    'nom' => 'azertyuiopqsdfghjklmwxcvbn AZERTYUIOPQSDFGHJKLMWXCVBN azertyuiopqsdfghjklmwxcvbn',
	    'prenom' => 'AZERTYUIOPQSDFGHJKLMWXCVBN azertyuiopqsdfghjklmwxcvbn AZERTYUIOPQSDFGHJKLMWXCVBN',
);
$entity = new Personne($datas2);
var_dump($entity);

echo '<h3>Création Personne avec données invalides-correctes via le validator</h3>';
$entityV = $validator->createValidEntity($datas2);
var_dump($entityV);

echo '<h3>Création Personne avec données fausses directement</h3>';
$datas3 = array(
	    'id_pers' => 'Toto',
	    'nom' => true,
	    'prenom' => '<h1>AZERTYUIOPQSDFGHJKLMWXCVBN</h1>',
);
$entity = new Personne($datas3);
var_dump($entity);
echo '<h3>Création Personne avec données fausses via le validator</h3>';
$entityV = $validator->createValidEntity($datas3);
var_dump($entityV);

$datas4 = array(
	    'nom' => NULL,
	    'prenom' => '<h1>AZERTYUIOPQSDFGHJKLMWXCVBN</h1>',
);
echo '<h3>Création Personne avec données fausses directement</h3>';
$entity = new Personne($datas4);
var_dump($entity);
echo '<h3>Création Personne avec données invalides-correctes via le validator</h3>';
try {
    $entityV = $validator->createValidEntity($datas4);
var_dump($entityV);
}
 catch (Phaln\Exceptions\ValidatorException $e)
 {
     var_dump($e);
 }


