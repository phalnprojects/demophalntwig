<?php
require_once '../config/appConfig.php';


$loader = new \Twig\Loader\FilesystemLoader(TEMPLATE_PATH);

$twig = new \Twig\Environment($loader);
$template = $twig->load('firstTwigTemplate.html.twig');

echo $template->render(['number' => 99]);