<?php
require_once '../config/appConfig.php';

use Phaln\GenericViewTwig;


//$view = new GenericViewTwig('default\default.html.twig');

$pers = new App\Entities\Personne(array('id'=>5, 'nom'=>'Machin', 'prenom'=>'truc'));
$_SESSION['user'] = $pers;

$view = new GenericViewTwig('firstTwigTemplate.html.twig');
$view->render(['message' => 'Pour le fun...', 'number' => 99]);	
