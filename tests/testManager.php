<?php
require_once '../config/testsConfig.php';

echo '<h1>Création App\Entities\Auteur</h1>';
$datas = array(
	    'idPersonne' => 22,
	    'idOuvrage' => 1,
	    'commentaire' => 'Commentaire 22',
);
$entity = new App\Entities\Auteur($datas);
dump_var($entity, DUMP, 'App\Entities\Auteur');

echo '<h3>Getter</h3>';
$pers = $entity->getPersonne();
dump_var($pers, DUMP, 'Personne de $entity');
$ouv = $entity->getOuvrage();
dump_var($ouv, DUMP, 'Ouvrage de $entity');


echo '<h1>Création Back\Entities\Auteur</h1>';
$datas = array(
	    'idPersonne' => 11,
	    'idOuvrage' => 2,
	    'commentaire' => 'Commentaire 1',
);
$entity2 = new Back\Entities\Auteur($datas);
dump_var($entity, DUMP, 'Back\Entities\Auteur');

echo '<h3>Getter</h3>';
$pers = $entity2->getPersonne();
dump_var($pers, DUMP, 'Personne de $entity');
$ouv = $entity2->getOuvrage();
dump_var($ouv, DUMP, 'Ouvrage de $entity');
