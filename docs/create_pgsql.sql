DROP TABLE IF EXISTS ouvrage CASCADE ;
CREATE TABLE ouvrage (
  id  serial NOT NULL,
  titre character varying(100) NOT NULL,
  annee integer DEFAULT NULL
);

DROP TABLE IF EXISTS personne CASCADE ;
CREATE TABLE personne (
    id_pers serial NOT NULL,
    nom character varying(30) NOT NULL,
    prenom character varying(30)
);

DROP TABLE IF EXISTS auteur CASCADE ;
CREATE TABLE auteur (
  idPersonne integer NOT NULL,
  idOuvrage integer NOT NULL,
  commentaire character varying(100) DEFAULT NULL
);

ALTER TABLE ONLY ouvrage
    ADD CONSTRAINT ouvrage_pkey PRIMARY KEY (id);

ALTER TABLE ONLY personne
    ADD CONSTRAINT personne_pkey PRIMARY KEY (id_pers);

ALTER TABLE ONLY auteur
    ADD CONSTRAINT auteur_pkey PRIMARY KEY (idPersonne, idOuvrage);
ALTER TABLE ONLY auteur
    ADD CONSTRAINT auteur_idPersonne_fkey FOREIGN KEY (idPersonne) REFERENCES personne(id_pers);
ALTER TABLE ONLY auteur
    ADD CONSTRAINT auteur_idOuvrage_fkey FOREIGN KEY (idOuvrage) REFERENCES ouvrage(id);

ALTER SEQUENCE ouvrage_id_seq restart  6;
ALTER SEQUENCE personne_id_pers_seq restart  32;
