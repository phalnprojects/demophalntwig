INSERT INTO ouvrage (id, titre, annee) VALUES
(1, 'Pour le meilleur et pour l''an pire', 2005),
(2, 'La vie va co vid', 2020),
(3, 'Amour, Prozac et autres curiosités ', 1997),
(4, 'Idées noires', 1977),
(5, 'Super Dupont', 1975);

INSERT INTO personne (id_pers, nom, prenom) VALUES
(8, 'O''GARAG', 'Mélodie'),
(9, 'ENCEQUENDISELONG', 'Cécile'),
(11, 'OCHON', 'Paul'),
(16, 'ASSOIE', 'Phil'),
(17, 'DUCABLE', 'Jean-Raoul'),
(19, 'DELAMAIN', 'Marc'),
(20, 'CULAIRE', 'Laurie'),
(22, 'DEUF', 'John'),
(23, 'DEUJOUR', 'Adam'),
(24, 'ZETOFRÉ', 'Mélanie'),
(25, 'NETOFRIGO', 'Jessica'),
(26, 'ONCITERNE', 'Camille'),
(28, 'CLETTE', 'Lara'),
(29, 'TALUE', 'Jean'),
(30, 'FRAICHIT', 'Sarah'),
(31, 'SOURCE', 'Aude');

INSERT INTO auteur (idPersonne, idOuvrage, commentaire) VALUES
(16, 3, 'Rien'),
(22, 1, 'Bof...'),
(22, 2, 'Machin'),
(22, 4, 'Texte'),
(25, 1, 'Blabla'),
(25, 2, 'Bidon'),
(28, 3, 'Tralala'),
(30, 4, NULL);
