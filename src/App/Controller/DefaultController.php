<?php
namespace App\Controller;
use App\View\VueDefaultDefault;
use Phaln\IController;

/**
 * Description of DefaultController
 *
 * @author phaln
 */
class DefaultController implements IController 
{
    /**
     * L'action par défaut. Affiche la page d'accueil.
     */
    public function DefaultAction(array $param = null)
    {
	$view = new VueDefaultDefault('default\default.html.twig');
	$view->render(['message' => 'Pour le fun...']);	
    }
}
