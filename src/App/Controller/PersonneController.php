<?php

namespace App\Controller;

use Phaln\IController;
use \App\Validators\PersonneValidator;
use App\View\VuePersonneShowAll;
use App\View\VuePersonneEdit;
use Phaln\Exceptions\EntityException;
use Phaln\Exceptions\ControllerException;

/**
 * Classe chargée de la gestion des personnes.
 *
 * @author phaln
 */
class PersonneController  implements IController {
    /**
     * L'action par défaut. Affiche la page d'accueil.
     * @param array $param les paramètres reçus
     */
    public function DefaultAction(array $param = null) {
	$this->ShowAllAction($param);
    }

    /**
     * Récupère toutes les personnes et les affiche.
     * @param array $param les paramètres reçus
     */
    public function ShowAllAction(array $param = null) {
	//  Récupération des personnes avec le repository
	$repo = \Phaln\Manager::getRepository('Personne');
	dump_var($repo, DUMP, 'Le PersonneRepository:');

	$allPers = $repo->getAll();
	if(DUMP)dump_var($allPers);
	
	$view = new VuePersonneShowAll('personne\showAll.html.twig');
	$view->render(['personnes' => $allPers]);	

    }

    /**
     * Edit d'une personne.
     * Fonctionne pour l'ajout/modification d'une nouvelle personne. 
     * Si on arrive en GET sans id de personne, affichage du formulaire vide.
     * Si on arrive en GET avec un id de personne, affichage du formulaire rempli 
     * avec les données de la personne.
     * Si on arrive en POST avec des données, c'est qu'un formulaire d'ajout a été complété.
     * @param array $param les paramètres reçus
     * @throws EntityException
     * @throws ControllerException
     */
    public function EditAction(array $param = null) {
	try {
	    //  Récupération de la méthode http utilisée (GET ou POST)
	    //  ATTENTION!! La ligne ci-dessous ne fonctionne pas pour certaines configurations sous Linux...
	    //  Cela pose problème lors de l'hébergement.
	    //  Voir http://demophaln.phaln.info/tests/testFilterInput.php
//	    $method = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING);
	    //  Correction du bug ci-dessus.
	    if (filter_has_var(INPUT_SERVER, "REQUEST_METHOD")) {
		$method = filter_input(INPUT_SERVER, "REQUEST_METHOD", FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
	    } else {
		if (isset($_SERVER["REQUEST_METHOD"]))
		    $method = filter_var($_SERVER["REQUEST_METHOD"], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
		else
		    $method = null;
	    }
	    dump_var($_SERVER['REQUEST_METHOD'], DUMP, 'Méthode http dans SERVER:');
	    dump_var($param, DUMP, 'EditAction $param:');
	    
	    switch (strtoupper($method)) {
		case 'GET':
		    // Demande d'ajout ou modification, affichage du formulaire
		    $personne = null;

		    // Récupération de l'id de la personne
		    $id = (isset($param['id'])) ? ($tmp = filter_var($param['id'], FILTER_VALIDATE_INT)) ? $tmp : null : null;
		    dump_var($id, DUMP, 'id reçu et filtré:');
		    
		    //  Si modification d'une personne demandée
		    if ($id) {
			//  Extraction de la personne depuis la bdd
			$repo = \Phaln\Manager::getRepository('Personne');
			$personne = $repo->getById($id);
			dump_var($personne, DUMP, 'Personne:');
			if (!$personne)
			    throw new EntityException('Personne inexistante.');
		    }

		    //  Affichage du formulaire, éventuellement rempli avec les données de la personne à modifier, sinon vierge (cas de l'ajout).
		    $view = new VuePersonneEdit('personne\edit.html.twig');
		    $view->render(['pers' => $personne, 
			    'displayId' => ($personne)?'':'none',
			    'action' => ($personne)?'Modifier':'Ajouter',]);	


		    break;

		case 'POST':
		    //  Le formulaire a été rempli... on le traite.
		    //  On crée une personne validée par le validateur.
		    if($param['id_pers'] === '')
			unset($param['id_pers']);
		    $validator = new PersonneValidator();
		    $entity = $validator->createValidEntity($param);
		    $entity->setNom(strtoupper($entity->getNom()));
		    dump_var($entity, DUMP, 'Personne validée:');

		    //  Sauvegarde de la personne dans la bdd
		    $repo = \Phaln\Manager::getRepository('Personne');
		    $entity = $repo->save($entity);
		    dump_var($entity, DUMP, 'Personne enregistrée:');
		    if (!DUMP)
			header("location: " . URL_BASE . "personneShowAll");
		    break;

		default:
		    throw new ControllerException('Méthode non prise ne charge.');
		    break;
	    }
	} catch (Throwable $ex) {
	    $controller = new App\Controller\ErrorController('Problème dans PersonneController:EditAction.<br/>' . $ex->getMessage());
	    $controller->DefaultAction();
	}
    }

    /**
     * Supprime une personne.
     * @param array $param les paramètres reçus
     * @throws EntityException
     * @throws ControllerException
     */
    public function DeleteAction(array $param = null) {
	try {
	    $personne = null;
	    $res = FALSE;

	    // Récupération de l'id de la personne
	    $id = ($tmp = filter_var($param['id'], FILTER_VALIDATE_INT)) ? $tmp : null;
	    dump_var($id, DUMP, 'id reçu et filtré:');

	    //  Si effacement d'une personne demandée
	    if ($id) {
		//  Extraction de la personne depuis la bdd
		$repo = \Phaln\Manager::getRepository('Personne');
		dump_var($repo, DUMP, 'Le PersonneRepository:');
		$personne = $repo->getById($id);
		dump_var($personne, DUMP, 'Personne à effacer:');
		
		//  Si on était rigoureux, on demanderait une confirmation...
		
		//  Difficile de supprimer une personne qui n'existe pas.
		if (!$personne)
		    throw new EntityException('Personne inexistante.');
		$res = $repo->deleteEntity($personne);
		if (!$res) {
		    throw new ControllerException('Erreur de supression.');
		} else {
		    if(!DUMP) header("location: " . URL_BASE . "personneShowAll");
		}
	    }
	} catch (Throwable $ex) {
	    $controller = new App\Controller\ErrorController('Problème dans PersonneController:DeleteAction.<br/>' . $ex->getMessage());
	    $controller->DefaultAction();
	}
    }

}
