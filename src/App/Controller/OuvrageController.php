<?php
namespace App\Controller;

use Phaln\Manager;
use Phaln\GenericViewTwig;

/**
 * Description of LivreController
 *
 * @author phaln
 */
class OuvrageController {
    /**
     * L'action par défaut. Affiche la page d'accueil.
     * @param array $param les paramètres reçus
     */
    public function DefaultAction(array $param = null) {
	$this->ShowAllAction($param);
    }

    /**
     * Récupère toutes les personnes et les affiche.
     * @param array $param les paramètres reçus
     */
    public function ShowAllAction(array $param = null) {
	//  Récupération des personnes avec le repository
	$repo = Manager::getRepository('Ouvrage');
	dump_var($repo, DUMP, 'OuvrageController::ShowAllAction() $repo:');

	$all = $repo->getAll();
	dump_var($all, DUMP, 'OuvrageController::ShowAllAction() $all ouvrages:');
	
	$view = new GenericViewTwig('ouvrage\showAll.html.twig');
	$view->render(['ouvrages' => $all]);	

    }
}
