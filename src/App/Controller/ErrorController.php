<?php
namespace App\Controller;
use Phaln\IController;
use App\View\VueErrorDefault;

/**
 * Description of ErrorController
 *
 * @author phaln
 */
class ErrorController  implements IController
{
    protected $msg = '';

    /**
     * Constructeur
     */
    public function __construct($msg)
    {
	$this->msg = $msg;
    }

    /**
     * L'action par défaut. Affiche la page d'erreur.
     */
    public function DefaultAction(array $param = null)
    {
	$view = new VueErrorDefault('error\default.html.twig');
	$view->render(['message' => $this->msg]);	
    }
}
