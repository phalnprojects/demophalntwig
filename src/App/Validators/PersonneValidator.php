<?php
namespace App\Validators;
use App\Entities\Personne;
use Phaln\AbstractValidator;

/**
 * Description of PersonneValidator
 *
 * @author phaln
 */
class PersonneValidator extends AbstractValidator
{
    public function __construct()
    {
	//  Les filtres pour les attributs
    	$attributFilters = array(
	    'id' => FILTER_VALIDATE_INT,
	    'nom' => FILTER_SANITIZE_STRING,
	    'prenom' => FILTER_SANITIZE_STRING,
	    );
	//  Les tailles des attributs
	$attributSizes = array(
		'nom' => 30,
		'prenom' => 30,
	);
	//  Les attributs obligatoires
	$attributRequired = array(
		'nom' => 'required',
	);
	parent::__construct(array('Filters'=>$attributFilters, 'Sizes' => $attributSizes, 'Required' => $attributRequired));
    }

    public function createValidEntity(?array $datas): ?\Phaln\AbstractEntity
    {
	$personne = $this->validateEntity(new Personne($datas));
	return $personne;
    }
}
