<?php
namespace App\Entities;
use Phaln\AbstractEntity;

/**
 * Auteur d'un ouvrage, entité de la base de données.
 *
 * @author Philippe
 */
class Auteur extends AbstractEntity
{
    //  Les attributs, ils correspondent aux champs de la table
    protected $idPersonne = null;
    protected $idOuvrage = null;
    protected $commentaire = null;
    
    //  Les attributs privés ne sont pas des champs de la BDD 
    //  et ne sont pas pris en charge par AbstractEntity.
    private $personne = null;	//  Entité de classe Personne correspondant à idPersonne
    private $ouvrage = null;	//  Entité de classe Ouvrage correspondant à idOuvrage
       
    /**
     * Getter de $personne
     * @return \App\Entities\Personne
     */
    public function getPersonne() : Personne
    {
	//  Si $personne est null, on le récupère par le PesonneRepository donné par le Manager
	if(is_null($this->personne)) {
	    $this->personne = \Phaln\Manager::getRepository('Personne')->getById($this->idPersonne);
	}
	return $this->personne;
    }
        
    /**
     * Getter de $ouvrage
     * @return \App\Entities\Ouvrage
     */
    public function getOuvrage() : Ouvrage
    {
	//  Si $ouvrage est null, on le récupère par le OuvrageRepository donné par le Manager
	if(is_null($this->ouvrage)) {
	    $this->ouvrage = \Phaln\Manager::getRepository('Ouvrage')->getById($this->idOuvrage);
	}
	return $this->ouvrage;
    }
}