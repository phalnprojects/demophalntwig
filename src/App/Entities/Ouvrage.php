<?php
namespace App\Entities;
use Phaln\AbstractEntity;

/**
 * Description d'un ouvrage, entité de la base de données.
 *
 * @author Philippe
 */
class Ouvrage extends AbstractEntity
{
    //  Les attributs, ils correspondent aux champs de la table
    protected $id = null;
    protected $titre = '';
    protected $annee = null;
    
    /**
     * Les auteurs de l'ouvrage
     * @var array de Auteur 
     */
    private $auteurs = null;
     
    /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid($val=null) :self
    {
	if($this->id === null)
	    $this->id = $val;
	return $this;
    }
       
    /**
     * Initialise et retourne la liste des auteurs
     * @return array|null
     */
    public function getAuteurs() : ?array
    {
	if(is_null($this->auteurs)) {
	    $auteurs = \Phaln\Manager::getRepository('Auteur')
				    ->getBy(array(
						array('fieldName' => 'idOuvrage',
						    'comp'=>'=',
						    'value'=>$this->id)
						)
					    );
	    if(!is_array($auteurs) && !is_null($auteurs))
		$this->auteurs[] = $auteurs;
	    else
		$this->auteurs = $auteurs;
	}
	return $this->auteurs;
    }
}