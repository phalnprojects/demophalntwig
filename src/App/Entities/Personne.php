<?php
namespace App\Entities;
use Phaln\AbstractEntity;

/**
 * Description d'une Personne, entité de la base de données.
 *
 * @author Philippe
 */
class Personne extends AbstractEntity
{
    //  Les attributs, ils correspondent aux champs de la table
    protected $id_pers = null;
    protected $nom = '';
    protected $prenom = null;
     
    //  Les attributs privés, ajoutés pour une raison quelquonque, 
    //  NE SONT PAS pris en compte par les méthodes issues de AbstractEntity...
    //  Vous devez alors implémenter celles qui vous intéresse.
    private $testPrivate = 'Privé, n\'est pas un champ';

    /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setid_pers($val=null) :self
    {
	if($this->id_pers === null)
	    $this->id_pers = $val;
	return $this;
    }
    
    // Ces deux méthodes ne sont nécessaire QUE si la clé primaire dans la table
    // porte un nom différent de id...
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @param type $val
     */
    public function setid($val=null) :self
    {
	return $this->setid_pers($val);
    }
    
    /**
     * Permet de s'affranchir du fait que le champ id dans la table est différent...
     * @return type
     */
    public function getid()
    {
	return $this->getid_pers();
    }
}