<?php
namespace App\Repositories;

use Phaln\AbstractRepository;
use Phaln\AbstractEntity;

/**
 * Description of OuvrageRepository
 *
 * @author Philippe
 */
class OuvrageRepository extends AbstractRepository
{	
    /**
     * Constructeur.
     * Fixe le nom des attributs.
     * Attention au nom de la clé primaire: par défaut c'est 'id'.
     */
    public function __construct()
    {
	parent::__construct();
	$this->table = 'ouvrage';
	$this->classMapped = 'App\Entities\Ouvrage';
	$this->idFieldName = 'id';
    }

    public function sauver(AbstractEntity $entity) {
	
    }

}
