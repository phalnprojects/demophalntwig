<?php

namespace App\View;

use Phaln\AbstractViewTwig;

/**
 * Vue d'erreur, utilisée par lors d'une erreur controleur ou action.
 * Hérite de AbstractVueTwig.
 *
 * @author Philippe
 */
class VueErrorDefault extends AbstractViewTwig {

    public function __construct($fichierTemplate) {
	dump_var($fichierTemplate, DUMP, 'VueErrorDefault(fichierTemplate):');
	parent::__construct($fichierTemplate);
    }

    public function render(array $datas) {
	echo $this->template->render($datas);
    }

}
