<?php

namespace App\View;

use Phaln\AbstractViewTwig;

/**
 * Vue d'accueil, utilisée par l'action par défaut du contrôleur par défaut.
 * Hérite de AbstractVueTwig.
 *
 * @author Philippe
 */
class VueDefaultDefault extends AbstractViewTwig {

    public function __construct($fichierTemplate) {
	dump_var($fichierTemplate, DUMP, 'VueDefaultDefault(fichierTemplate):');
	parent::__construct($fichierTemplate);
    }

    public function render(array $datas) {
	echo $this->template->render($datas);
    }

}
