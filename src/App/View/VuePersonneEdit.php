<?php

namespace App\View;

use Phaln\AbstractViewTwig;
use App\Entities\Personne;

/**
 * Vue utilisée par l'action Edit du contrôleur Personne.
 * Hérite de AbstractVueTwig.
 *
 * @author Philippe
 */
class VuePersonneEdit extends AbstractViewTwig {

    public function __construct($fichierTemplate) {
	dump_var($fichierTemplate, DUMP, 'VuePersonneEdit(fichierTemplate):');
	parent::__construct($fichierTemplate);
    }

    public function render(array $datas) {
	echo $this->template->render($datas);
    }

}
