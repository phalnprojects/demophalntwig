<?php

namespace App\View;

use Phaln\AbstractViewTwig;

/**
 * Vue utilisée par l'action ShowAll du contrôleur Personne.
 * Hérite de AbstractVueTwig.
 *
 * @author Philippe
 */
class VuePersonneShowAll extends AbstractViewTwig {

    protected $content = "";  // Contenu qui sera ajouté au template courant

    public function __construct($fichierTemplate) {
	dump_var($fichierTemplate, DUMP, 'VuePersonneShowAll(fichierTemplate):');
	parent::__construct($fichierTemplate);
    }

    public function render(array $datas) {
	echo $this->template->render($datas);
    }

}
