<?php
namespace App\View;
use Phaln\Template;

/**
 * Description of Page
 *
 * @author Philippe
 */
class Page extends Template
{
    protected $_headerFile = TEMPLATE_PATH;
    protected $_mainFile = TEMPLATE_PATH;
    protected $_footerFile = TEMPLATE_PATH;

    public function __construct($fichierTemplate)
    {
	$this->_headerFile .= 'header.inc.tpl';
	dump_var($this->_headerFile, DUMP, 'HeaderFile:');
	$this->_mainFile .= 'menu.inc.tpl';
	$this->_footerFile .= 'footer.inc.tpl';
	$urlSite = '<a href="'.filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_URL).'">DemoMVC</a>';
	$_header = new Template($this->_headerFile);
	$_header->set('nomSite', $urlSite);
	$_main = new Template($this->_mainFile);
	$_footer = new Template($this->_footerFile);

	parent::__construct($fichierTemplate);
	$this->set('header', $_header)
		->set('main', $_main)
		->set('footer', $_footer);
    }
}
