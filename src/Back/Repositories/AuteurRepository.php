<?php
namespace App\Repositories;

use Phaln\AbstractRepository;
use Phaln\AbstractEntity;

/**
 * Description of AuteurRepository
 *
 * @author Philippe
 */
class AuteurRepository extends AbstractRepository
{	
    /**
     * Constructeur.
     * Fixe le nom des attributs.
     * Attention au nom de la clé primaire: par défaut c'est 'id'.
     */
    public function __construct()
    {
	parent::__construct();
	$this->table = 'auteur';
	$this->classMapped = 'Back\Entities\Auteur';
	$this->idFieldName = ['idPersonne', 'idOuvrage'];
    }

    public function sauver(AbstractEntity $entity) {
	
    }

}
