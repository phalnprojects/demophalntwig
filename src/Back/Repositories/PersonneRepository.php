<?php
namespace Back\Repositories;

use Phaln\AbstractRepository;
use Phaln\AbstractEntity;

/**
 * Description of PersonneRepository
 *
 * @author Philippe
 */
class PersonneRepository extends AbstractRepository
{	
    /**
     * Constructeur.
     * Fixe le nom des attributs.
     * Attention au nom de la clé primaire: par défaut c'est 'id'.
     */
    public function __construct()
    {
	parent::__construct();
	$this->table = 'personne';
	$this->classMapped = 'Back\Entities\Personne';
	$this->idFieldName = 'id_pers';
    }
	    
}
