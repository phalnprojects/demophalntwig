<?php
//  Permet d'utiliser le typage fort si strict_types=1
//  !! Laisser en première ligne
declare(strict_types=1);

//  Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
define('DUMP', TRUE);

//  Les informations de connexion à la BDD
$infoBdd = array(
		'interface' => 'pdo',
		'type'   => 'mysql',
		'host'   => '',
		'port'   =>  3306,	    // 5432 pour postgreSQL, 3306 pour MySQL
		'charset' => 'UTF8',
		'dbname' => '',
		'user'   => '',
		'pass'   => '',
	);


require_once 'globalConfig.php';
