<?php
//  Permet d'utiliser le typage fort si strict_types=1
//  !! Laisser en première ligne
declare(strict_types=1);

//  Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
define('DUMP', TRUE);

//  Le titre à donner aux pages du site
define('TITRE_APP', "Démo Phaln");
define('VERSION_APP', 0.1);


//  L'url de votre site, sera utile dans les pages en cas de déplacement du site...
define('URL_BASE', "http://host/myWebSite/");


//  Vos informations de connexion à la BDD
$infoBdd = array(
		'interface' => 'pdo',	    // "pdo" ou "mysqli"
		'type'   => 'mysql',	    //  mysql ou pgsql
		'host'   => '',
		'port'   =>  3306,	    // Par défaut: 5432 pour postgreSQL, 3306 pour MySQL
		'charset' => 'UTF8',
    		'dbname' => '',
		'user'   => '',
		'pass'   => '',
	);

require_once 'globalConfig.php';

//  Réglage du nom de fichier pour logger les exceptions
if(class_exists('Phaln\Utility\FileLogger'))
{
    Phaln\Utility\FileLogger::$defaultFileName = LOG_DIR . 'demoPhaln_log.txt';
}

//  Réglage du nom de l'espace de nom dans lequel se trouve les espaces de nom Entities et Repositories du model de l'application
if(class_exists('Phaln\Manager'))
{
    Phaln\Manager::setDefaultModelNameSpace('App');
}


//  Lancement des sessions, si ce n'est pas déjà fait.
//  Le faire après l'inclusion de 'globalConfig.php' permet d'avoir l'autoload actif
//  et de pouvoir désérializer des objets depuis les sessions.
if(session_status() === PHP_SESSION_NONE) session_start();