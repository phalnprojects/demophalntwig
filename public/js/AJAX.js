function InitPage()
{
    ModifModele();
}

$(function() {
    $("#modele").change(ModifModele);
    
    $("#tCarte").change(function() {
        //recupere select Modele
        var childIndex=document.getElementById("modele").selectedIndex;
        var selectedModele=document.getElementById("modele").children[childIndex];
        
        var tposX=selectedModele.dataset.posx;
        var tposY=selectedModele.dataset.posy;
        
        
        //recupere select Carte
        var childIndex=document.getElementById("tCarte").selectedIndex;
        var selectedCarte=document.getElementById("tCarte").children[childIndex];
        
        var largeurImg=selectedCarte.dataset.largeur;
        var hauteurImg=selectedCarte.dataset.hauteur;
        
        var largeurTexte=formatZoneTexte(largeurImg, tposX);
        var hauteurTexte=formatZoneTexte(hauteurImg, tposY);
        
        $("#blocImg").css({'width': largeurImg, 'height': hauteurImg});
        $("#prevCom").css({'height': hauteurTexte, 'width':largeurTexte});
        $("#texte").css({'width':largeurTexte});
        /*image.style.width=children[i].dataset.largeur+'px';
        image.style.height=children[i].dataset.hauteur+'px';*/
    });
    
    $("#police").change(function(){
        var childIndex=document.getElementById("police").selectedIndex;
        var police=document.getElementById("police").children[childIndex].text;

        $("#prevCom").css({'font-family': police});
    });
    
    $("#gras").click(function(){
        if($('#prevCom').css('fontWeight')=="400")
        {
            $("#prevCom").css({'fontWeight': "bold"});
            $("#hgras").val(true);
        }
        else
        {
            $("#prevCom").css({'fontWeight': ""});
            $("#hgras").val(false);
        }
    });
    $("#italic").click(function(){
        if($('#prevCom').css('fontStyle')=="normal")
        {
            $("#prevCom").css({'fontStyle': "italic"});
            $("#hitalique").val(true);
        }
        else
        {
            $("#prevCom").css({'fontStyle': "normal"});
            $("#hitalique").val(false);
        }
    });
    $("#souligne").click(function(){
        if($('#prevCom').css('textDecoration')=="none")
        {
            $("#prevCom").css({'textDecoration': "underline"});
            $("#hsouligne").val(true);
        }
        else
        {
            $("#prevCom").css({'textDecoration': "none"});
            $("#hsouligne").val(false);
        }
    });
    
    $("#align").change(function(){
        $("#prevCom").css({'text-align': $(this).val()});
    });

    $("#taille").change(function(){
        var newSize= document.getElementById('taille').value;
        $('#prevCom').css('font-size', newSize+'px');
    });

    $("#texte").keyup(function() {
        var textarea=document.getElementById('texte');
        var texte=document.getElementById('prevCom');
        texte.innerHTML=$(this).val().replace(/(?:\\[rn]|[\r\n]+)+/g, '<br>');
    });
    
    $("#papier").change(function(){
        var childIndex=document.getElementById("papier").selectedIndex;
        var selectedPapier=document.getElementById("papier").children[childIndex];
        var prixPapier=parseFloat(selectedPapier.dataset.prix);
        
        var quantite= parseInt(document.getElementById('quantite').value);
        var newPrix=prixPapier*quantite;
        
        document.getElementById("prix").innerHTML=newPrix;
    });
    $("#quantite").change(function(){
        var childIndex=document.getElementById("papier").selectedIndex;
        var selectedPapier=document.getElementById("papier").children[childIndex];
        var prixPapier=parseFloat(selectedPapier.dataset.prix);
        
        var quantite= parseInt(document.getElementById('quantite').value);
        var newPrix=prixPapier*quantite;
        
        document.getElementById("prix").innerHTML=newPrix;
    });
});

function formatZoneTexte(taille, pos)
{ return taille-pos; }


function ModifModele()
{
    //alert(document.getElementById("modele").selectedItem.value);

    /*var childIndex=document.getElementById("modele").selectedIndex;
     var selectedModele=document.getElementById("modele").children[childIndex];*/
    var tposX=$("#modele option:selected").data("posx");
    var tposY=$("#modele option:selected").data("posy");

    //mise a jour select des type de carte
    $("#tCarte").val($("#modele option:selected").data("idcarte"));
    //document.getElementById('tCarte').value=$("#modele option:selected").data("idcarte");

    //mise a jour du select des polices
    document.getElementById('police').value=$("#modele option:selected").data("idpolice");

    //mise a jour du select metier
    document.getElementById('metier').value=$("#modele option:selected").data("idmetier");
    //alert("val: "+document.getElementById('modele').value);


    //verifie le style de la police(gras, italique, souligne)
    var fontWeight, fontStyle, textDecoration;
    if($("#modele option:selected").data("gras")==='t')
    { fontWeight="bold"; }
    else { fontWeight=""; }

    if($("#modele option:selected").data("italic")==='t')
    { fontStyle="italic"; }
    else { fontStyle=""; }

    if($("#modele option:selected").data("souligne")==='t')
    { textDecoration="underline"; }
    else { textDecoration=""; }


    //création de l'url de l'image
    var imgURL="url(img/"+$("#modele option:selected").data("nomimg")+")";

    //recuperation de la police
    var police=$("#police option:selected").text();

    //dimension de la carte selectionner
    var largeurImg=$("#tCarte option:selected").data("largeur");
    var hauteurImg=$("#tCarte option:selected").data("hauteur");

    //calcul des dimensions des zone de Texte (largeur et hauteur)
    var largeurTexte=formatZoneTexte(largeurImg, tposX);
    var hauteurTexte=formatZoneTexte(hauteurImg, tposY);

    //mise a jour de l'image
    $("#blocImg").css({'background-image':imgURL, 'height': hauteurImg, 'width': largeurImg});
    //mise a jour de la largeur du textarea
    $("#texte").css({'width':largeurTexte});
    //mise a jour de la previsualtion
    $("#prevCom").css({'width': largeurTexte, 'height': hauteurTexte, 'font-family': police, 'fontWeight': fontWeight, 'fontStyle':fontStyle, 'textDecoration':textDecoration, 'left': parseInt(tposX), 'top': parseInt(tposY)});

    //mise a jour des value des input hidden
    //hidden image et zone texte
    $("#nomimg").val($("#modele option:selected").data("nomimg"));
    $("#X").val(tposX);
    $("#Y").val(tposY);
    //hidden style du texte
    $("#hgras").val($("#modele option:selected").data("gras"));
    $("#hitalic").val($("#modele option:selected").data("italic"));
    $("#hsouligne").val($("#modele option:selected").data("souligne"));
}